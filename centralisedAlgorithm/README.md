# Compilation

```bash
g++ main.cpp -o a.out
```

# Execution

```bash
./a.out [node] [capacity units] [ring?]
```

where: 

node: (Integer) number of nodes of the virtual network requested

capacity units: (Integer) Capacity units of physical nodes
 
ring: (1 o 0) Is it a ring or a bus? 