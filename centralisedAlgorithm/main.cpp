#include <cstdlib>
#include <vector>

#include "simulator.hpp"

typedef struct {
  int id0;
  float rank1;
  int cu;
  float length;
} r;

int unitsCU;
int numberOfNodes = 14;
int flagBlock = -1;
int count = 0;
int nodes;
int ring;
char top;

void swap(r* a1, r* a2) {
  r a = *a1;
  *a1 = *a2;
  *a2 = a;
}
std::vector<r> ranking = std::vector<r>(numberOfNodes);
std::vector<int> connectionsPerNode(numberOfNodes, 0);

void initRanking() {
  for (int i = 0; i < ranking.size(); i++) {
    ranking[i].id0 = i;
    ranking[i].cu = unitsCU;
  }
  return;
}

void runRanking(std::vector<int> linksPerNode,
                std::vector<int> freeSlotsPerNode,
                std::vector<float> lengthsPerNode) {
  int id;
  for (int i = 0; i < ranking.size(); i++) {
    id = ranking[i].id0;
    ranking[i].rank1 = freeSlotsPerNode[id] / ((float)linksPerNode[id]);
    ranking[i].length = lengthsPerNode[id] / ((float)linksPerNode[id]);
  }
  return;
}

void sortRanking() {
  for (int i = 0; i < ranking.size(); i++) {
    for (int j = 0; j < ranking.size(); j++) {
      if (ranking[j - 1].rank1 > ranking[j].rank1)
        swap(&ranking[j - 1], &ranking[j]);
      else if (ranking[j - 1].rank1 == ranking[j].rank1 &&
               ranking[j - 1].length > ranking[j].length)
        swap(&ranking[j - 1], &ranking[j]);
    }
  }
}

std::vector<int> findSpace3(int number) {
  int minCU = 1;
  std::vector<int> s = std::vector<int>();
  int j = 0;
  for (int i = 0; i < number; i++) {
    for (; j < ranking.size(); j++) {
      if (ranking[j].cu >= minCU) {
        s.push_back(ranking[j].id0);
        j++;
        break;
      }
    }
  }

  return s;
}

void subtractCU(int id) {
  for (int i = 0; i < ranking.size(); i++) {
    if (ranking[i].id0 == id) {
      ranking[i].cu -= 1;
      return;
    }
  }
}

BEGIN_ALLOC_FUNCTION(FirstFit) {
  int numberOfSlots = REQ_SLOTS(0);
  int currentNumberSlots;
  int currentSlotIndex;
  std::vector<bool> totalSlots;
  std::vector<int> freeSlotsPerNode(numberOfNodes, 0);
  std::vector<int> linksPerNode(numberOfNodes, 0);
  std::vector<float> lengthsPerNode(numberOfNodes, 0);

  int numberOfLinks = this->network->getNumberOfLinks();
  std::vector<std::vector<bool> > slots =
      std::vector<std::vector<bool> >(numberOfLinks);

  for (int i = 0; i < slots.size(); i++) {
    slots[i] = std::vector<bool>(LINK_IN_ROUTE(0, 0)->getSlots(), false);
  }

  for (int i = 0; i < numberOfLinks; i++) {
    int id = this->network->getLink(i)->getSrc();
    int lenght = this->network->getLink(i)->getLength();
    int sizeSlots = this->network->getLink(i)->getSlots();
    int countSlots = 0;
    for (int n = 0; n < sizeSlots; n++) {
      if (this->network->getLink(i)->getSlot(n) == true) {
        countSlots += 1;
      }
    }
    freeSlotsPerNode[id] += countSlots;
    linksPerNode[id] += 1;
    lengthsPerNode[id] += lenght;
  }

  runRanking(linksPerNode, freeSlotsPerNode, lengthsPerNode);
  sortRanking();
  std::vector<int> space3 = findSpace3(nodes);

  if (space3.size() < nodes) {
    printf("\n%c,%i,%i\n", top, nodes, count);
    exit(count);
  }
  for (int w = 0; w < space3.size(); w++) {
    if (space3[w] == -1) {
      if (flagBlock == -1) {
        printf("\n%c,%i,%i\n", top, nodes, count);
        exit(count);
      }

      return NOT_ALLOCATED;
    }
  }

  for (int i = 0; i < space3.size(); i++) {
    subtractCU(space3[i]);
  }
  int flagA = 0;

  for (int w = 0; w < 2 * (space3.size() - 1 + ring); w++) {
    if (w % 2 == 0) {
      src = space3[w / 2];
      dst = space3[(w / 2 + 1) % space3.size()];
    } else {
      dst = space3[w / 2];
      src = space3[(w / 2 + 1) % space3.size()];
    }

    flagA = 0;
    for (int i = 0; i < 1; i++) { 
      totalSlots = std::vector<bool>(LINK_IN_ROUTE(0, 0)->getSlots(), false);
      for (int j = 0; j < NUMBER_OF_LINKS(i); j++) {
        for (int k = 0; k < LINK_IN_ROUTE(i, j)->getSlots(); k++) {
          totalSlots[k] = totalSlots[k] | LINK_IN_ROUTE(i, j)->getSlot(k) |
                          slots[LINK_IN_ROUTE_ID(i, j)][k];
        }
      }
      currentNumberSlots = 0;
      currentSlotIndex = 0;
      for (int j = 0; j < totalSlots.size(); j++) {
        if (totalSlots[j] == false) {
          currentNumberSlots++;
        } else {
          currentNumberSlots = 0;
          currentSlotIndex = j + 1;
        }
        if (currentNumberSlots == numberOfSlots) {
          for (int j = 0; j < NUMBER_OF_LINKS(i); j++) {
            ALLOC_SLOTS(LINK_IN_ROUTE_ID(i, j), currentSlotIndex,
                        numberOfSlots);
            for (int k = currentSlotIndex; k < currentSlotIndex + numberOfSlots;
                 k++) {
              slots[LINK_IN_ROUTE_ID(i, j)][k] = true;
            }
          }
          flagA = 1;
          break;
        }
      }
      if (flagA == 1) {
        break;
      }
    }
    if (flagA != 1) {
      printf("\n%c,%i,%i\n", top, nodes, count);
      exit(count);
    }
  }

  count += 1;
  return ALLOCATED;
}
END_ALLOC_FUNCTION

int main(int argc, char* argv[]) {
  Simulator sim = Simulator(std::string("json_files/NSFNet.json"),
                            std::string("json_files/routes.json"),
                            std::string("json_files/bitrates.json"));
  USE_ALLOC_FUNCTION(FirstFit, sim);
  sim.setGoalConnections(1e3);
  sim.setLambda(100000);
  sim.init();

  nodes = atoi(argv[1]);
  unitsCU = atoi(argv[2]);
  ring = atoi(argv[3]);
  if (ring == 1) {
    top = 'r';
  } else {
    top = 'b';
  }
  initRanking();
  sim.run();
  return 0;
}
